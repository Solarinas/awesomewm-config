-- Awesome libraries
local gears = require('gears')
local beautiful = require('beautiful')
local awful = require('awful')

-- Default shell
awful.util.shell = 'zsh'

-- Theme
beautiful.init(require('theme'))

-- Layout
require('layout')

-- Configurations
require('configuration.client')
require('configuration.tags')
root.keys = (require('configuration.keys.global'))

-- Modules
require('module.auto-start')
require('module.notifications')
-- require('module.volume-osd')

-- Wallpaper
screen.connect_signal(
   'request::wallpaper',
   function(s)
      if beautiful.wallpaper then
	 if type(beautiful.wallpaper) == 'string' then
	    if beautiful.wallpaper:sub(1, #'#') == '#' then
	       gears.wallpaper.set(beautiful.wallpaper)
	    elseif beautiful.wallpaper:sub(1, #'/') == '/' then
	       gears.wallpaper.maximized(beautiful.wallpaper, s)
	    end
	 else
	    beautiful.wallpaper(s)
	 end
      end
   end	       
)
