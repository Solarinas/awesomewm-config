-- Awesome Libraries
local awful = require('awful')
local beautiful = require('beautiful')

local client_keys = require('configuration.client.keys')
local client_buttons = require('configuration.client.buttons')

-- Rules to apply to new clients
 awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = client_keys,
                     buttons = client_buttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap + awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {},
        class = {
          "Gnome-calculator",
	  "Alacritty",
	  "Wine",
	  "explorer.exe",
	  "mpv",
	  "Tor Browser",
          "xtightvncviewer"},
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},
    { rule = { class = "Emacs" },
     properties = { size_hints_honor = false } },
}
