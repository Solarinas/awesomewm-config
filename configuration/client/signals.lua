-- Awesome Libraries
local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')

-- Signal to round and unround fullscreen apps
local rrect = function(radius)
   return function(cr, width, height)
      gears.shape.rounded_rect(cr, width, height, radius)
   end
end

local function no_round_corners (c)
   if c.fullscreen then
      c.shape = gears.shape.rectangle
   else
      c.shape = rrect(beautiful.border_radius)
   end
end

client.connect_signal("property::fullscreen", no_round_corners)

-- Signal for screensavers
local fullscreen_clients = {}

local function remove_client(tabl, c)
   local index = awful.util.table.hasitem(tabl, c)
   if index then
      table.remove(tabl, index)
      if #tabl == 0 then
	 awful.spawn("xset s on")
	 awful.spawn("xset +dpms")
      end
   end
end

client.connect_signal("property::fullscreen",
		      function(c)
			 if c.fullscreen then
			    table.insert(fullscreen_clients, c)
			    if #fullscreen_clients == 1 then
			       awful.spawn("xset s off")
			       awful.spawn("xset -dpms")
			    end
			 else
			    remove_client(fullscreen_clients, c)
			 end
end)

client.connect_signal("unmanage",
    function(c)
        if c.fullscreen then
            remove_client(fullscreen_clients, c)
        end
    end)
		      

-- Signal function to execute when a new client appears
client.connect_signal(
   "manage",
   function (c)
      -- Set the windows at the slave,
      -- i.e put it at the end of others instead of setting it master
      c.shape = rrect(beautiful.border_radius)
      if not awesome.startup then
	 awful.client.setslave(c)
      end
      
      if awesome.startup
	 and not c.size_hints.user_position
      and not c.size_hints.program_position then
	 -- Prevent clients from being unreachable after screen count changes
	 awful.placement.no_offscreen(c)
      end
end)

-- Enable sloppy focus so that focus follows mouse
client.connect_signal(
   "mouse::enter",
   function (c)
      c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

-- Prevent windows from minimizing
client.connect_signal("property::minimized", function(c)
    c.minimized = false
end)

-- Prevent windows from maximizing 
client.connect_signal("property::maximized", function(c)
    c.maximized = false
end)


