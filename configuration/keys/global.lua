-- Load Awesome Libraries
local awful = require("awful")

require('awful.autofocus')


local modkey = require('configuration.keys.mod').mod_key
local altkey = require('configuration.keys.mod').alt_key
local apps = require('configuration.apps')

-- Key Bindings
local globalKeys = awful.util.table.join(

   -- Reload Awesome
   awful.key({modkey, 'Shift'}, 'r', awesome.restart,
      {description = 'reload awesome', group = 'awesome'}),

   -- Quit Awesome
   awful.key({modkey, 'Shift'}, 'e', awesome.quit,
      {description = 'quit awesome', group = 'awesome'}),

   -- Launchers
   awful.key({modkey}, 'Return', function() awful.spawn(apps.default.terminal) end,
      {description = 'Open a terminal', group = 'launcher'}),
   -- Open text editor
   awful.key({modkey}, 'e', function() awful.spawn(apps.default.editor) end,
      {description = "Open a text editor", group = 'launcher'}),
   -- Shortcuts to open terminal applications
   awful.key({modkey}, 'y', function() awful.spawn(apps.default.terminal .. " -e btm") end,
      {description = 'Open system monitor', group = 'launcher'}),
   awful.key({modkey}, 'r', function() awful.spawn(apps.default.terminal .. " -e ranger") end,
      {description = 'Open file manager', group = 'launcher'}),
   awful.key({modkey}, 't', function() awful.spawn(apps.default.terminal .. " -e stig") end,
      {description = 'Open Transmission', group = 'launcher'}),
   awful.key({modkey}, 'm', function() awful.spawn(apps.default.terminal .. " -e ncmpcpp") end,
      {description = 'Open Music Player', group = 'launcher'}),


   -- Rofi
   awful.key({modkey}, 'd', function() awful.spawn(apps.default.rofi) end,
      {description = 'Open app launcher', group = 'rofi'}),
   awful.key({modkey}, 'p', function() awful.spawn('bwmenu') end,
      {description = "Password Manager", group = "rofi"}),
   awful.key({modkey}, 'u', function() awful.spawn('rofimoji') end,
      {description = "Emoji Selection", group = "rofi"}),


   -- Change layouts
   awful.key({modkey}, 'space', function ()  awful.layout.inc(1) end,
      {description = "select next", group = "layout"}),
   awful.key({modkey, 'Shift'}, 'space', function ()  awful.layout.inc(1) end,
      {description = "select next", group = "layout"}),

   -- Volume control
   awful.key({}, 'XF86AudioLowerVolume',
      function()
	 awful.spawn('amixer -D pulse sset Master 5%-', false)
	 awesome.emit_signal('widget::volume')
	 awesome.emit_signal('module::volume_osd:show', true)
      end,
      {description = "Descrease the volume", group = 'hotkeys'}),
   awful.key({}, 'XF86AudioRaiseVolume',
      function()
	 awful.spawn('amixer -D pulse sset Master 5%+', false)
	 awesome.emit_signal('widget::volume')
	 awesome.emit_signal('module::volume_osd:show', true)
      end,
      {description = "Increase the volume", group = 'hotkeys'}),
   awful.key({}, 'XF86AudioMute', function() awful.spawn('amixer -D pulse Master 1+ toggle', false) end,
      {description = 'Toggle mute', group = 'hotkeys'}),

   -- Media keys
   awful.key({}, 'XF86AudioPlay', function() awful.spawn('playerctl play-pause') end,
      {descripton = 'Play/Pause media', group = 'hotkeys'}),
   awful.key({}, 'XF86AudioNext', function() awful.spawn('playerctl next') end,
      {descripton = 'Play next', group = 'hotkeys'}),
   awful.key({}, 'XF86AudioPrev', function() awful.spawn('playerctl previous') end,
      {descripton = 'Play previous', group = 'hotkeys'}),
   awful.key({}, 'XF86AudioStop', function() awful.spawn('playerctl stop') end,
      {descripton = 'Stop media', group = 'hotkeys'}),

   -- Screenshot
   awful.key({}, 'Print', function() awful.spawn('screenshot -u') end,
      {description = 'Screenshot window', group = 'hotkeys'}),
   awful.key({'Shift'}, 'Print', function() awful.spawn('screenshot -s') end,
       {description = 'Screenshot selection', group = 'hotkeys'}),
   awful.key({modkey}, 'Print', function() awful.spawn('screenshot -m') end,
      {description = 'Screenshot screen', group = 'hotkeys'})

)

for i = 1, 9 do
    globalKeys = awful.util.table.join(globalKeys,
        -- View tags only
        awful.key({ modkey }, "#" .. i + 9,
            function ()
                --Tag back and forth
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then 
                    tag:view_only()
                end
            end,
            {description = "view tag #" ..i, group = "tag"}),
    -- Toggle tag display
    awful.key({ modkey, 'Control' }, "#" .. i + 9,
        function ()
            local screen = awful.screen.focused()
            local tag = screen.tags[i]
            if tag then
                awful.tag.viewtoggle(tag)
            end
        end,
        {description = "toggle tag #" .. i, group = "tag"}),

    -- Move client to tag
    awful.key({ modkey, 'Shift' }, "#" .. i + 9,
        function ()
            if client.focus then
                local tag = client.focus.screen.tags[i]
                if tag then
                    client.focus:move_to_tag(tag)
                end
            end
        end,
        {description = "move focues client to tag #" ..i, group = "tag"}),

        -- Toggle tag on focused client
        awful.key({ modkey, ctl , 'Shift' }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

return globalKeys
