-- Load Awesome Libraries
local beautiful = require('beautiful')
local gears = require('gears')

local filesystem = gears.filesystem
local dpi = beautiful.xresources.apply_dpi
local gtk_variable = beautiful.gtk.get_theme_variables

local theme_dir = filesystem.get_configuration_dir() .. '/theme'

-- Create theme table
local theme = {}

-- Fonts
theme.font = 'Inconsolata Bold 10'
theme.font_bold = 'Inconsolata Bold 10'

-- Menu icon them
theme.icon_theme = 'Mkos-Big-Sur'

local awesome_overrides = function(theme)
   theme.dir = theme_dir
   theme.icons = theme_dir .. '/icons/'

   -- Default Wallpaper
   theme.wallpaper = theme_dir .. '/wallpapers/bg1.png'

   -- Foreground
   theme.fg_normal = '#ffffffde'
   theme.fg_focus = '#e4e4e4'
   theme.fg_urgent = '#CC9393'

   theme.bg_normal = theme.background
   theme.bg_focus = '#5a5a5a'
   theme.bg_urgent = '#3F3F3F'

   -- System tray
   theme.bg_systray = theme.background
   theme.systray_icon_spacing = dpi(16)

   -- UI Groups
   theme.groups_title_bg = '#ffffff' .. '15'
   theme.groups_bg = '#ffffff' .. '10'
   theme.groups_radius = dpi(16)

   -- Borders
   theme.border_focus = gtk_variable().bg_color
   theme.border_normal = gtk_variable().base_color
   theme.border_marked = '#CC9393'
   theme.border_width = dpi(0)
   theme.border_radius = dpi(12)

   -- Decorations
   theme.client_radius = dpi(9)
   theme.useless_gap = dpi(7)

   -- Tooltips

   theme.tooltip_bg = theme.background
   theme.tooltip_border_color = theme.transparent
   theme.tooltip_border_width = 0
   theme.tooltip_gaps = dpi(5)
   theme.tooltip_shape = function(cr, w, h)
      gears.shape.rounded_rect(cr, w, h, dpi(6))
   end

   -- Notification
   theme.notification_position = 'top_right'
   theme.notificaiton_bg = theme.transparent
   theme.notificaiton_margin = dpi(5)
   theme.notification_border_width = dpi(0)
   theme.notification_border_color = theme.transparent
   theme.notification_spacing = dpi(12)
   theme.notification_icon_resize_strategy = 'center'
   theme.notification_icon_size = dpi(64)
   theme.notification_shape = function(cr, w, h)
      gears.shape.rounded_rect(cr, w, h, dpi(9))
   end

end

return {
   theme = theme,
   awesome_overrides = awesome_overrides
}
