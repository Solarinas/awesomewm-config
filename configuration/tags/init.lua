-- Awesome Libraries
local awful = require('awful')
local beautiful = require('beautiful')

awful.layout.layouts = {
   awful.layout.suit.tile,
   awful.layout.suit.floating,
   awful.layout.suit.fair
}

awful.screen.connect_for_each_screen(
   function(s)
      awful.tag({"1", "2", "3", "4", "5" }, s, awful.layout.layouts[1])
   end
)
awful.tag({ "1", "2", "3", "4", "5"} , s, awful)

tag.connect_signal(
   'property::layout',
   function(t)
      local currentLayout = awful.tag.getproperty(t, 'layout')
      t.gap = beautiful.useless_gap
   end
)

-- Focus on urgent clients
awful.tag.attached_connect_signal(
   s,
   'property::selected',
   function ()
      local urgent_clients = function (c)
	 return awful.rules.match(c, {urgent = true})
      end
      for c in awful.client.iterate(urgent_clients) do
	 if c.first_tag == mouse.screen.selected_tag then
	    client.focus = c
	    c:raise()
	 end
      end
   end
)
