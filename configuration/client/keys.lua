-- Awesome Libraries
local awful = require('awful')

require('awful.autofocus')

local modkey = require('configuration.keys.mod').mod_key
local altkey = require('configuration.keys.mod').alt_key

-- Window Managment Keys
local clientKeys = awful.util.table.join(

   -- Close client
   awful.key({modkey}, "q", function (c) c:kill() end,
      {description = "close", group = "client"}),
   
   -- Change focused client
   awful.key({modkey}, "j", function () awful.client.focus.byidx(1) end,
      {description = "focus next by index", group = "client"}),
   awful.key({modkey}, "k", function () awful.client.focus.byidx(-1) end,
      {description = "focus previous by index", group = "client"}),

   -- Move focused client
   awful.key({modkey,  "Shift" }, "j", function () awful.client.swap.byidx(1) end,
      {description = "swap with next client by index", group = "client"}),
   awful.key({modkey,  "Shift" }, "k", function () awful.client.swap.byidx(-1) end,
      {description = "swap with previous client by index", group = "client"}),

   -- Fullscreen window
   awful.key({modkey}, "f",
      function (c)
	 c.fullscreen = not c.fullscreen
	 c:raise()
      end,
      {description = "toggle fullscreen", group = "client"}),

   -- Toggle floating
   awful.key({modkey}, "s",
      function(c)
	 c.fullscreen = false
	 c.floating = not c.floating
      end,
      {description = "toggle floating", group = "client"})
   
)

return clientKeys
