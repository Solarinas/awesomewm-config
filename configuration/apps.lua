return {
   -- Default Applications
   default = {
      terminal = "kitty",
      editor = "emacsclient -nc",
      web_browser = "brave-nightly",
      rofi = 'rofi -show drun'
   },

   -- Run Applications on start up
   run_on_start_up = {
      "picom -b --experimental-backends --dbus --config ~/.picom.conf",
      "numlockx on",
      "mpd",
      "mpDris2",
      "wal -R",
      "redshift",
      "ckb-next --background",
      "transmission-daemon",
      "udiskie",
      "xsettingsd"
   }
}
