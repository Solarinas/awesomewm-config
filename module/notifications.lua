local awful = require('awful')
local naughty = require("naughty")
local ruled = require('ruled')
local gears = require('gears')
local beautiful = require('beautiful')
local dpi = beautiful.xresources.apply_dpi

-- Defaults
naughty.config.defaults.ontop = true
naughty.config.defaults.icon_size = dpi(32)
naughty.config.defaults.timeout = 5
naughty.config.defaults.title = 'System Notification'
naughty.config.defaults.margin = dpi(16)
naughty.config.defaults.border_width = 0
naughty.config.defaults.position = 'top_left'
naughty.config.defaults.shape = function(cr, w, h)
	gears.shape.rounded_rect(cr, w, h, dpi(6))
end

-- {{{ Notifications

ruled.notification.connect_signal(
   'request::rules',
   function()

      -- All notifications will match this rule.
      ruled.notification.append_rule {
	 rule       = { urgency = 'normal' },
	 properties = {
	    font = 'Inconsolata Bold 10',
	    bg = beautiful.transparent,
	    fg = beautiful.fg_normal,
	    margin = dpi(16),
	    position = 'top_right',
	    implicit_timeout = 5,
	 }
      }

      -- Critical notifications
      ruled.notification.append_rule {
	 rule = { urgency = 'critical' },
	 properties = {
	    font = 'Inconsolata Bold 10',
	    bg = beautiful.transparent,
	    fg = beautiful.fg_normal,
	    margin = dpi(16),
	    position = 'top_right',
	    implicit_timeout= 0
	 }
      }

      -- Low notifications
      ruled.notification.append_rule {
	 rule = { urgency = 'low'},
	 properties = {
	    font = 'Inconsolata Bold 10',
	    bg = beautiful.transparent,
	    fg = beautiful.fg_normal,
	    margin = dpi(16),
	    position = 'top_right',
	    implicit_timeout = 5
	 }
      }
      
end)

-- Error Handling
naughty.connect_signal(
   'request::display_error',
   function(message, startup)
      naughty.notification {
	 urgency = 'critical',
	 title = 'Oh no! You made a boo-boo',
	 message = message,
	 app_name = 'System Notifcation'
      }
   end   
)

naughty.connect_signal("request::display", function(n)
    naughty.layout.box { notification = n }
end)

-- }}}

