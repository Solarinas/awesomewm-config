local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')
local beautiful = require('beautiful')

local dpi = beautiful.xresources.apply_dpi

local top_panel = function(s)
   local panel = wibox
   {
      ontop = true,
      screen = s,
      type = 'dock',
      height = dpi(28),
      width = s.geometry.width,
      x = s.geometry.x,
      y = s.geometry.y,
      stretch = false,
      bg = beautiful.background,
      fg = beautiful.fg_normal
   }

   panel:struts
   {
      top = dpi(28)
   }

   panel:connect_signal(
      'mouse::enter',
      function()
	 local w = mouse.current_wibox
	 if w then
	    w.cursor = 'left_ptr'
	 end
      end
   )
   s.systray = wibox.widget {
      visible = false,
      base_size = dpi(20),
      horizontal = true,
      screen = 'primary',
      widget = wibox.widget.systray
   }

   -- Clock
   mytextclock = wibox.widget.textclock()
   s.mypromptbox = awful.widget.prompt()
   s.mytaglist = awful.widget.taglist {
      screen = s,
      filter = awful.widget.taglist.filter.all,
      buttons = taglist_buttons
   }
   s.mytasklist = awful.widget.tasklist {
      screen = s,
      filter = awful.widget.tasklist.filter.currenttags,
      buttons = tasklist_buttons
   }

   panel : setup {
      layout = wibox.layout.align.horizontal,
      expand = 'none',
      {
	 layout = wibox.layout.align.horizontal,
	 s.mytaglist,
	 s.mypromptbox,
      },
      s.mytasklist,
      {
	 layout = wibox.layout.fixed.horizontal,
	 s.systray,
	 mytextclock,
	 
      }
		 }
   return panel
end
return top_panel
